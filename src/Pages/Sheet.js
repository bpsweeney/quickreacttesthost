import React, {useState, useEffect} from 'react';
import './CSS/style.css';
import './CSS/Home.css';
import Nav from './components/Navbar';
import { useParams } from 'react-router-dom';
//import APDF from './Sp.pdf'

function Sheet(){
    let {id} =  useParams()
    const [APDF, setPdf] = useState({ path: ''})
    let error = "File could not be found. Try refreshing or uploading the wav again."
    //Make code here to make a call to the backend with the SheetId.
    useEffect(() => {
        fetch('/getPDF/' + id, {
            method: 'GET',
            //headers : { 
            //    'Content-Type': 'application/pdf',
            //    'Accept': 'application/pdf'
            //}
        }).then(response => response.json())
                                    //Will change upon hosting, but will be very similar.
        .then(message => setPdf({path:'http://127.0.0.1:5000/putPDFonpage' + message}))
        .catch((error) =>{
            console.log(error)
            const Err = document.getElementById("Error");
            Err.style.opacity = 1;
        });
    }, [id]);    

    ////////////////////////////////////////////////////////////////
    return(
        <div>
        <section>
            <Nav />
        </section>
        <div className='container'>
            {/* Any filler content you want at the top of the page
                put it here.
            {id} {APDF.path}*/}
        </div>
        <div className= 'container'>
        <div className="error" id="Error">
            {error}
        </div>
            {/* This doesn't exactly work.
            <a href={APDF.path} download>Click here to download</a>*/}    
        </div>
        <div className = 'container'>
            <object className = "marg-top" title= "ThePDF" data= {APDF.path} type="application/pdf" width="100%" height="900">
                {/*<p>Here is a link to the pdf <a href={APDF.path}>to the PDF!</a></p>*/}
            </object>
        </div>
        </div>
    )
}

export default Sheet;